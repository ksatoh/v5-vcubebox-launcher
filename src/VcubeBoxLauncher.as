
package {
	
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.utils.Timer;
	
	import jp.co.vcube.ascore3.foundation.LanguageManager;
	import jp.co.vcube.centre.stb.Consts;
	import jp.co.vcube.centre.stb.Texts;
	import jp.co.vcube.centre.stb.core.MainModelLocator;
	import jp.co.vcube.centre.stb.core.NativeWindowKeepActivater;
	import jp.co.vcube.centre.stb.core.StbApplication;
	import jp.co.vcube.centre.stb.core.VBoxLauncherModel;
	import jp.co.vcube.centre.stb.core.VBoxLauncherUtilitySettingLoader;
	import jp.co.vcube.centre.stb.events.ApplicationShutdownEvent;
	import jp.co.vcube.centre.stb.framework.core.VCSSFocusManager;
	import jp.co.vcube.centre.stb.modalScreens.DuringPowerOffModalScreen;
	import jp.co.vcube.centre.stb.screens.LauncherScreen;
	import jp.co.vcube.centre.stb.screens.view.BackgroundImage;
	import jp.co.vcube.centre.stb.skins.Embed;
	import jp.co.vcube.centre.stb.skins.SkinManager;
	import jp.co.vcube.centre.stb.skins.SkinManagerTypeClassImpl;
	import jp.co.vcube.centre.stb.utils.UIConfig;
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
	import jp.co.vcube.mobile.framework.components.VCAlert;
	import jp.co.vcube.mobile.framework.core.VCApplication;
	import jp.co.vcube.mobile.framework.core.VCApplicationModalViewStackItem;
	import jp.co.vcube.mobile.framework.core.VCOSType;
	import jp.co.vcube.mobile.framework.core.VCScreen;
	import jp.co.vcube.mobile.framework.core.VCUtil;
	
	[SWF(width="1920", height="1080")]
	public class VcubeBoxLauncher extends VCScreen {
		
		private var app:StbApplication;
		private var model:VBoxLauncherModel;
		private var waitLoginScreenTimer:Timer;
		private var keepActivater:NativeWindowKeepActivater;
		
		public function VcubeBoxLauncher() {
			
			backgroundVisible = true;
			backgroundColors = [0x000000];
			
			loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR,
				function(event:UncaughtErrorEvent):void{
					VCubeBoxUtil.log(event.error,event.target);
					if (event.error is Error)
						VCubeBoxUtil.log( Error(event.error).message);
					else if (event.error is ErrorEvent )
						VCubeBoxUtil.log( ErrorEvent(event.error).text );
					else
						VCubeBoxUtil.log( event.error.toString() );
					event.preventDefault();
				});
			
			var fontName:String = UIConfig.FONTFAMILY_DEFAULT;
			if ( VCUtil.checkOS( VCOSType.MAC ) || VCUtil.checkOS( VCOSType.IOS ) )
				fontName = UIConfig.FONTFAMILY_MAC;
			else if ( VCUtil.checkOS( VCOSType.WIN ) )
				fontName = UIConfig.FONTFAMILY_WIN;
			
			UIConfig.fontSizeNormal = 24;
			UIConfig.fontSizeSmall = 20;
			UIConfig.fontSizeLarge = 28;
			UIConfig.fontSizeBig = 36;
			
			VCUtil.DEFAULT_FONT = fontName;
			VCUtil.DEFAULT_FONT_SIZE = VCubeBoxUtil.fontSizeNormal;
			VCUtil.DEFAULT_FONT_BOLD = UIConfig.fontBoldNormal;
			
			model = VBoxLauncherModel.getInstance(this);
			model.backgroundImage = new BackgroundImage(this);
			model.utilityLoader = VBoxLauncherUtilitySettingLoader.getInstance();
			model.addEventListener(ApplicationShutdownEvent.WILL_SHUTDOWN,willShutdownHandler);
			
			app = new StbApplication(this);
			VCSSFocusManager.create( stage );
			MainModelLocator.getInstance().stage = stage;
			MainModelLocator.getInstance().skinManager = new SkinManager( new SkinManagerTypeClassImpl(), new LanguageManager(), Embed );
			keepActivater = NativeWindowKeepActivater.getInstance();
			
			addEventListener(Event.ADDED_TO_STAGE,addToStageHandler);
		}
		
		protected function addToStageHandler(event:Event):void
		{
			VCubeBoxUtil.log("did added to stage");
			
			(model.utilityLoader as VBoxLauncherUtilitySettingLoader).addEventListener(IOErrorEvent.IO_ERROR,loadErrorSettingFileHandler);
			(model.utilityLoader as VBoxLauncherUtilitySettingLoader).addEventListener(SecurityErrorEvent.SECURITY_ERROR,loadErrorSettingFileHandler);
			(model.utilityLoader as VBoxLauncherUtilitySettingLoader).addEventListener(Event.COMPLETE,loadCompleteSettingFileHandler);
			model.utilityLoader.load( Consts.UTILITY_SETTING_PATH );
		}
		
		protected function loadCompleteSettingFileHandler(event:Event):void
		{
			VCubeBoxUtil.log("completed loading setting files");
			
			stage.nativeWindow.addEventListener(Event.CLOSING,windowClosingHandler);
			stage.addEventListener(FullScreenEvent.FULL_SCREEN,didFullScreenHandler);
			stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			stage.scaleMode = StageScaleMode.NO_SCALE;
		}
		
		protected function loadErrorSettingFileHandler(event:Event):void { 
			
			removeSettingFileHandler();
			VCAlert.show("Error","Failed to load setting file.");
		}
		
		protected function removeSettingFileHandler():void {
			
			(model.utilityLoader as VBoxLauncherUtilitySettingLoader).removeEventListener(IOErrorEvent.IO_ERROR,loadErrorSettingFileHandler);
			(model.utilityLoader as VBoxLauncherUtilitySettingLoader).removeEventListener(SecurityErrorEvent.SECURITY_ERROR,loadErrorSettingFileHandler);
			(model.utilityLoader as VBoxLauncherUtilitySettingLoader).removeEventListener(Event.COMPLETE,loadCompleteSettingFileHandler);
		}
		
		protected function didFullScreenHandler(event:FullScreenEvent):void
		{
			VCubeBoxUtil.log("did FullScreen");
			stage.removeEventListener(FullScreenEvent.FULL_SCREEN,didFullScreenHandler);
			
			keepActivater.target = stage;
			keepActivater.start();
			
			//起動時にキーボード操作がされているとキーボードのフォーカスが取れない問題の対策として、起動直後に一度最前面に表示する
			function _onWaitLoginScreenTimerEventHandler(event:TimerEvent):void
			{
				VCubeBoxUtil.log("force activate window");
				keepActivater.forceActivateWidnow();
				waitLoginScreenTimer.removeEventListener(TimerEvent.TIMER,_onWaitLoginScreenTimerEventHandler);
			}
			waitLoginScreenTimer = new Timer(1000,2);
			waitLoginScreenTimer.addEventListener(TimerEvent.TIMER,_onWaitLoginScreenTimerEventHandler);
			waitLoginScreenTimer.addEventListener(TimerEvent.TIMER_COMPLETE,function(event:TimerEvent):void{ createLoginScreen(); });
			waitLoginScreenTimer.start();
		}
		
		protected function createLoginScreen():void {
			
			VCubeBoxUtil.log("create LauncherScreen");
			model.backgroundImage.show();
			var screen:LauncherScreen = new LauncherScreen();
			app.pushView( screen, true, VCApplication.STACK_TRANSITION_FROM_VERTICAL );
		}
		
		private function windowClosingHandler(event:Event):void
		{
			keepActivater.stop();
		}
		
		private function willShutdownHandler(event:ApplicationShutdownEvent):void
		{
			keepActivater.stop();
			var exists:Boolean = false;
			for each ( var item:VCApplicationModalViewStackItem in VCApplication.getInstance().modalViews ) {
				if ( item.view is DuringPowerOffModalScreen )
					exists = true;
			}
			if ( !exists )
				app.presentModalView( new DuringPowerOffModalScreen( Texts.DURING_POWER_OFF ) );
		}
	}
}
