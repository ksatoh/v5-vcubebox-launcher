package jp.co.vcube.centre.stb
{
	public class Texts
	{
		public static const DURING_POWER_OFF:String = "電源をオフにしています...";
		public static const EXEC_POWER_OFF_BUTTON_LABEL:String = "電源を切る";
		public static const CANCEL_BUTTON_LABEL:String = "キャンセル";
	}
}