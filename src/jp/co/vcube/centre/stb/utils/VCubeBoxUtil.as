package jp.co.vcube.centre.stb.utils
{
	import jp.co.vcube.ascore3.util.Utils;
	import jp.co.vcube.mobile.framework.core.VCUtil;

	public class VCubeBoxUtil
	{
		private static var _fullscreenScale:Number = 1;
		private static var _vcutilDefaultFontSize:Number = 0;
		public static function get fullscreenScale():Number { return _fullscreenScale; }
		public static function set fullscreenScale(value:Number):void {
			_fullscreenScale = value;
			if ( !_vcutilDefaultFontSize )
				_vcutilDefaultFontSize = VCUtil.DEFAULT_FONT_SIZE;
			VCUtil.DEFAULT_FONT_SIZE = _vcutilDefaultFontSize * _fullscreenScale;
		}
		
		public static function optimizeSize(value:Number):Number
		{
			return fullscreenScale * VCUtil.optimizeSize(value);
		}
		
		public static function get fontSizeNormal():uint { return Math.floor(fullscreenScale * UIConfig.fontSizeNormal); }
		public static function get fontSizeSmall():uint { return Math.floor(fullscreenScale * UIConfig.fontSizeSmall); }
		public static function get fontSizeLarge():uint { return Math.floor(fullscreenScale * UIConfig.fontSizeLarge); }
		public static function get fontSizeBig():uint { return Math.floor(fullscreenScale * UIConfig.fontSizeBig); }
		
		public static function log(...rest):void
		{
			trace(Utils.getNowTimeString()+" "+rest);
		}
	}
}