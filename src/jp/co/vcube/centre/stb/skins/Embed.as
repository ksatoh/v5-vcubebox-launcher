
package jp.co.vcube.centre.stb.skins {
		
	public class Embed {
		
		[Embed(source="/assets/Spinner.swf#SpinnerLarge")]
		public static const spinnerLarge:Class;
		
		[Embed(source="/assets/background.png")]
		public static const backgroundImage:Class;
		
		[Embed(source="/assets/Skins.swf#background_Black")]
		public static const background_Black:Class;

		[Embed(source="/assets/Skins.swf#background_EnterMeeting")]
		public static const background_EnterMeeting:Class;

		[Embed(source="/assets/Skins.swf#background_PopupWindow")]
		public static const background_PopupWindow:Class;

		[Embed(source="/assets/Skins.swf#background_ReserveMeeting_disableSkin")]
		public static const background_ReserveMeeting_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#background_ReserveMeeting_downSkin")]
		public static const background_ReserveMeeting_downSkin:Class;

		[Embed(source="/assets/Skins.swf#background_ReserveMeeting_overSkin")]
		public static const background_ReserveMeeting_overSkin:Class;

		[Embed(source="/assets/Skins.swf#background_ReserveMeeting_upSkin")]
		public static const background_ReserveMeeting_upSkin:Class;

		[Embed(source="/assets/Skins.swf#background_RoomInformation_disableSkin")]
		public static const background_RoomInformation_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#background_RoomInformation_downSkin")]
		public static const background_RoomInformation_downSkin:Class;

		[Embed(source="/assets/Skins.swf#background_RoomInformation_overSkin")]
		public static const background_RoomInformation_overSkin:Class;

		[Embed(source="/assets/Skins.swf#background_RoomInformation_upSkin")]
		public static const background_RoomInformation_upSkin:Class;

		[Embed(source="/assets/Skins.swf#background_Userlist_disableSkin")]
		public static const background_Userlist_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#background_Userlist_downSkin")]
		public static const background_Userlist_downSkin:Class;

		[Embed(source="/assets/Skins.swf#background_Userlist_overSkin")]
		public static const background_Userlist_overSkin:Class;

		[Embed(source="/assets/Skins.swf#background_Userlist_upSkin")]
		public static const background_Userlist_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_BackScreen_disableSkin")]
		public static const button_BackScreen_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_BackScreen_downSkin")]
		public static const button_BackScreen_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_BackScreen_overSkin")]
		public static const button_BackScreen_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_BackScreen_upSkin")]
		public static const button_BackScreen_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Cancel_disableSkin")]
		public static const button_Cancel_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Cancel_downSkin")]
		public static const button_Cancel_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Cancel_overSkin")]
		public static const button_Cancel_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Cancel_upSkin")]
		public static const button_Cancel_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_InviteMembers_disableSkin")]
		public static const button_InviteMembers_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_InviteMembers_downSkin")]
		public static const button_InviteMembers_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_InviteMembers_overSkin")]
		public static const button_InviteMembers_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_InviteMembers_upSkin")]
		public static const button_InviteMembers_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Next_disableSkin")]
		public static const button_Next_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Next_downSkin")]
		public static const button_Next_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Next_overSkin")]
		public static const button_Next_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Next_upSkin")]
		public static const button_Next_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_PowerOff_disableSkin")]
		public static const button_PowerOff_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_PowerOff_downSkin")]
		public static const button_PowerOff_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_PowerOff_overSkin")]
		public static const button_PowerOff_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_PowerOff_upSkin")]
		public static const button_PowerOff_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Prev_disableSkin")]
		public static const button_Prev_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Prev_downSkin")]
		public static const button_Prev_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Prev_overSkin")]
		public static const button_Prev_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Prev_upSkin")]
		public static const button_Prev_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Primary_disableSkin")]
		public static const button_Primary_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Primary_downSkin")]
		public static const button_Primary_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Primary_overSkin")]
		public static const button_Primary_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_Primary_upSkin")]
		public static const button_Primary_upSkin:Class;

		[Embed(source="/assets/Skins.swf#button_StartMeeting_disableSkin")]
		public static const button_StartMeeting_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#button_StartMeeting_downSkin")]
		public static const button_StartMeeting_downSkin:Class;

		[Embed(source="/assets/Skins.swf#button_StartMeeting_overSkin")]
		public static const button_StartMeeting_overSkin:Class;

		[Embed(source="/assets/Skins.swf#button_StartMeeting_upSkin")]
		public static const button_StartMeeting_upSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_disableSkin")]
		public static const checkbox_disableSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_downSkin")]
		public static const checkbox_downSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_overSkin")]
		public static const checkbox_overSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_upSkin")]
		public static const checkbox_upSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_disableSelectedSkin")]
		public static const checkbox_disableSelectedSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_downSelectedSkin")]
		public static const checkbox_downSelectedSkin:Class;

		[Embed(source="/assets/Skins.swf#checkbox_selectedSkin")]
		public static const checkbox_selectedSkin:Class;

		[Embed(source="/assets/Skins.swf#icon_Avatar_NotSetting")]
		public static const icon_Avatar_NotSetting:Class;

		[Embed(source="/assets/Skins.swf#icon_OfflineStatus")]
		public static const icon_OfflineStatus:Class;

		[Embed(source="/assets/Skins.swf#icon_OnlineStatus")]
		public static const icon_OnlineStatus:Class;

		[Embed(source="/assets/Skins.swf#icon_RoomLock")]
		public static const icon_RoomLock:Class;

		[Embed(source="/assets/Skins.swf#icon_Selected")]
		public static const icon_Selected:Class;

		[Embed(source="/assets/Skins.swf#icon_UnderMeetingStatus")]
		public static const icon_UnderMeetingStatus:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Prev_disableSkin")]
		public static const room_button_Prev_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Prev_downSkin")]
		public static const room_button_Prev_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Prev_overSkin")]
		public static const room_button_Prev_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Prev_upSkin")]
		public static const room_button_Prev_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Next_disableSkin")]
		public static const room_button_Next_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Next_downSkin")]
		public static const room_button_Next_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Next_overSkin")]
		public static const room_button_Next_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Next_upSkin")]
		public static const room_button_Next_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Up_disableSkin")]
		public static const room_button_Up_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Up_downSkin")]
		public static const room_button_Up_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Up_overSkin")]
		public static const room_button_Up_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Up_upSkin")]
		public static const room_button_Up_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Down_disableSkin")]
		public static const room_button_Down_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Down_downSkin")]
		public static const room_button_Down_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Down_overSkin")]
		public static const room_button_Down_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Down_upSkin")]
		public static const room_button_Down_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Menu_disableSkin")]
		public static const room_button_Menu_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Menu_downSkin")]
		public static const room_button_Menu_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Menu_overSkin")]
		public static const room_button_Menu_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_Menu_upSkin")]
		public static const room_button_Menu_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#icon_File")]
		public static const icon_File:Class;
		
		[Embed(source="/assets/Skins.swf#icon_Folder")]
		public static const icon_Folder:Class;
		
		[Embed(source="/assets/Skins.swf#icon_IMG")]
		public static const icon_IMG:Class;
		
		[Embed(source="/assets/Skins.swf#icon_PPT")]
		public static const icon_PPT:Class;
		
		[Embed(source="/assets/Skins.swf#icon_XLS")]
		public static const icon_XLS:Class;
		
		[Embed(source="/assets/Skins.swf#icon_DOC")]
		public static const icon_DOC:Class;
		
		[Embed(source="/assets/Skins.swf#icon_PDF")]
		public static const icon_PDF:Class;
		
		[Embed(source="/assets/Skins.swf#icon_Drilldown")]
		public static const icon_Drilldown:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Prev_disableSkin")]
		public static const room_button_file_Prev_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Prev_DownSkin")]
		public static const room_button_file_Prev_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Prev_overSkin")]
		public static const room_button_file_Prev_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Prev_upSkin")]
		public static const room_button_file_Prev_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Next_disableSkin")]
		public static const room_button_file_Next_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Next_downSkin")]
		public static const room_button_file_Next_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Next_overSkin")]
		public static const room_button_file_Next_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Next_upSkin")]
		public static const room_button_file_Next_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Up_disableSkin")]
		public static const room_button_file_Up_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Up_downSkin")]
		public static const room_button_file_Up_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Up_overSkin")]
		public static const room_button_file_Up_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_file_Up_upSkin")]
		public static const room_button_file_Up_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#icon_MicMute")]
		public static const icon_MicMute:Class;
		
		[Embed(source="/assets/Skins.swf#CursorFinger")]
		public static const pointer_marker:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeUp_disableSkin")]
		public static const room_button_VolumeUp_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeUp_downSkin")]
		public static const room_button_VolumeUp_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeUp_overSkin")]
		public static const room_button_VolumeUp_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeUp_upSkin")]
		public static const room_button_VolumeUp_upSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeDown_disableSkin")]
		public static const room_button_VolumeDown_disableSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeDown_downSkin")]
		public static const room_button_VolumeDown_downSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeDown_overSkin")]
		public static const room_button_VolumeDown_overSkin:Class;
		
		[Embed(source="/assets/Skins.swf#room_button_VolumeDown_upSkin")]
		public static const room_button_VolumeDown_upSkin:Class;
	}
}
