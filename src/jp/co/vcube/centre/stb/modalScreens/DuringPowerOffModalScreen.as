package jp.co.vcube.centre.stb.modalScreens
{
	import jp.co.vcube.centre.stb.modalScreens.view.DuringPowerOffModalScreenDelegate;
	import jp.co.vcube.centre.stb.screens.BaseScreen;
	
	public final class DuringPowerOffModalScreen extends BaseScreen
	{
		public function DuringPowerOffModalScreen( info:String="" )
		{
			super();
			
			view.infoText = info;
		}
		
		override protected function prepareScreen():void {
			
			super.prepareScreen();
			
			flow = CF_V;
			align = CA_M;
		}
		
		override protected function createChildren():void {
			
			viewDelegate = new DuringPowerOffModalScreenDelegate(this);
			
			super.createChildren();
		}

		private function get view():DuringPowerOffModalScreenDelegate { return viewDelegate as DuringPowerOffModalScreenDelegate; }
	}
}