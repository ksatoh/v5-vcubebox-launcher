package jp.co.vcube.centre.stb.modalScreens.view
{
	import flash.display.DisplayObject;
	
	import jp.co.vcube.centre.stb.framework.ui.VCSSLabel;
	import jp.co.vcube.centre.stb.screens.view.ScreenDelegate;
	import jp.co.vcube.centre.stb.skins.Embed;
	import jp.co.vcube.centre.stb.utils.UIConfig;
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
	import jp.co.vcube.mobile.framework.core.VCContainer;
	import jp.co.vcube.mobile.framework.core.VCScreen;
	
	public final class DuringPowerOffModalScreenDelegate extends ScreenDelegate
	{
		private var mainContainer:VCContainer;
		private var spinner:DisplayObject;
		private var infoLabel:VCSSLabel;
		
		public function DuringPowerOffModalScreenDelegate(screen:VCScreen)
		{
			super(screen);
		}
		
		public override function exec():void
		{
			mainContainer = new VCContainer();
			
			infoLabel = createLabel("", UIConfig.fontColorNormal, true, VCubeBoxUtil.fontSizeBig);
			infoLabel.visible = false;
			
			spinner = new Embed.spinnerLarge();
			
			mainContainer.addChild(infoLabel);
			mainContainer.addChild(createSpacer());
			mainContainer.addChild(spinner);
			mainContainer.addChild(createSpacer());
			
			mainContainer.getChildAt( mainContainer.getChildIndex(infoLabel) + 1 ).visible = false;
			
			addChild(createSpacer());
			addChild(mainContainer);
			addChild(createSpacer( VCubeBoxUtil.optimizeSize(200), true ));
		}
		
		public function set infoText( info:String ):void
		{
			infoLabel.text = info;
			infoLabel.visible = Boolean(info);
			mainContainer.getChildAt( mainContainer.getChildIndex(infoLabel) + 1 ).visible = true;
			
			mainContainer.layout();
		}
	}
}

