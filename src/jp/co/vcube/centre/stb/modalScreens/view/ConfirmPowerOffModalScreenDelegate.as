package jp.co.vcube.centre.stb.modalScreens.view
{
	import flash.events.MouseEvent;
	
	import jp.co.vcube.centre.stb.Texts;
	import jp.co.vcube.centre.stb.framework.core.IVCSSFocusableContent;
	import jp.co.vcube.centre.stb.framework.ui.VCSSLabelButton;
	import jp.co.vcube.centre.stb.modalScreens.events.ConfirmPowerOffModalViewEvent;
	import jp.co.vcube.centre.stb.screens.view.ScreenDelegate;
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
	import jp.co.vcube.mobile.framework.core.VCContainer;
	import jp.co.vcube.mobile.framework.core.VCScreen;
	
	[Event(name="clickedCancelButton", type="jp.co.vcube.centre.stb.modalScreens.events.ConfirmPowerOffModalViewEvent")]
	[Event(name="clickedPowerOffButton", type="jp.co.vcube.centre.stb.modalScreens.events.ConfirmPowerOffModalViewEvent")]
	
	public final class ConfirmPowerOffModalScreenDelegate extends ScreenDelegate
	{
		private var powerOffButton:VCSSLabelButton;
		private var cancelButton:VCSSLabelButton;
		private var mainContainer:VCContainer;
		
		private function get WINDOW_WIDTH():uint { return VCubeBoxUtil.optimizeSize(800);}
		private function get WINDOW_HEIGHT():uint { return VCubeBoxUtil.optimizeSize(480);}
		private function get BUTTON_WIDTH():uint { return VCubeBoxUtil.optimizeSize(320); }
		private function get BUTTON_HEIGHT():uint { return VCubeBoxUtil.optimizeSize(120); }
		private function get MARGIN():uint { return VCubeBoxUtil.optimizeSize(50); }
		
		public function ConfirmPowerOffModalScreenDelegate(screen:VCScreen)
		{
			super(screen);
		}
		
		public override function exec():void
		{
			mainContainer = new VCContainer();
			mainContainer.size = 0;
			mainContainer.backgroundColors = [ 0xffffff ];
			mainContainer.backgroundAlphas = [ 0.85 ];
			mainContainer.backgroundVisible = true;
			mainContainer.flow = CF_V;
			mainContainer.align = CA_M;
			
			powerOffButton = createPrimaryLabelButton( Texts.EXEC_POWER_OFF_BUTTON_LABEL, BUTTON_WIDTH, BUTTON_HEIGHT, VCubeBoxUtil.fontSizeNormal );
			powerOffButton.addEventListener(MouseEvent.CLICK,clickedCancelInviteButtonHandler);
			
			cancelButton = createSecondryLabelButton( Texts.CANCEL_BUTTON_LABEL, BUTTON_WIDTH, BUTTON_HEIGHT, VCubeBoxUtil.fontSizeNormal );
			cancelButton.addEventListener(MouseEvent.CLICK,clickedCancelButtonHandler);
			
			mainContainer.addChild( createSpacer() );
			mainContainer.addChild( powerOffButton );
			mainContainer.addChild( createSpacer( VCubeBoxUtil.optimizeSize(5), true ) );
			mainContainer.addChild( cancelButton );
			mainContainer.addChild( createSpacer() );
			mainContainer.setSize( WINDOW_WIDTH, WINDOW_HEIGHT );
			
			addChild( createSpacer() );
			addChild( mainContainer );
			addChild( createSpacer() );
		}
		
		private function clickedCancelInviteButtonHandler(event:MouseEvent):void
		{
			dispatchEvent(new ConfirmPowerOffModalViewEvent(ConfirmPowerOffModalViewEvent.CLICKED_POWER_OFF_BUTTON));
		}
		
		private function clickedCancelButtonHandler(event:MouseEvent):void
		{
			dispatchEvent(new ConfirmPowerOffModalViewEvent(ConfirmPowerOffModalViewEvent.CLICKED_CANCEL_BUTTON));
		}
		
		public override function get focusableContents():Vector.<IVCSSFocusableContent>{
			
			var c:Vector.<IVCSSFocusableContent> = Vector.<IVCSSFocusableContent>( [
				powerOffButton,
				cancelButton
			] );
			return c;
		}
	}
}