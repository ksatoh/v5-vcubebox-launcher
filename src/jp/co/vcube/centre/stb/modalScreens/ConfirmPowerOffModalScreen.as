package jp.co.vcube.centre.stb.modalScreens
{
	import jp.co.vcube.centre.stb.core.PowerOffController;
	import jp.co.vcube.centre.stb.modalScreens.events.ConfirmPowerOffModalViewEvent;
	import jp.co.vcube.centre.stb.modalScreens.view.ConfirmPowerOffModalScreenDelegate;
	import jp.co.vcube.centre.stb.screens.BaseScreen;
	
	public final class ConfirmPowerOffModalScreen extends BaseScreen
	{
		public function ConfirmPowerOffModalScreen()
		{
			super();
		}
		
		override protected function prepareScreen():void {
			
			super.prepareScreen();
			
			flow = CF_V;
			align = CA_M;
		}
		
		override protected function createChildren():void {
			
			viewDelegate = new ConfirmPowerOffModalScreenDelegate(this);
			
			super.createChildren();
			
			view.addEventListener(ConfirmPowerOffModalViewEvent.CLICKED_CANCEL_BUTTON,clickedCancelButtonHandler);
			view.addEventListener(ConfirmPowerOffModalViewEvent.CLICKED_POWER_OFF_BUTTON,clickedPowerOffButtonHandler);
		}
		
		override public function operationCancel():void {
			
			app.dismissModalView( this, false );
		}
		
		private function clickedCancelButtonHandler(event:ConfirmPowerOffModalViewEvent):void
		{
			operationCancel();
		}
		
		private function clickedPowerOffButtonHandler(event:ConfirmPowerOffModalViewEvent):void
		{
			//表示箇所が多いため、例外的にモーダルスクリーン内で処理を実行します
			PowerOffController.getInstance().exec();
			operationCancel();
		}
		
		private function get view():ConfirmPowerOffModalScreenDelegate { return viewDelegate as ConfirmPowerOffModalScreenDelegate; }
	}
}