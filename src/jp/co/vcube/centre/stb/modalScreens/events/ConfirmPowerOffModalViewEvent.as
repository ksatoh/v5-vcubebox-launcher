package jp.co.vcube.centre.stb.modalScreens.events
{
	import flash.events.Event;
	
	public final class ConfirmPowerOffModalViewEvent extends Event
	{
		public static const CLICKED_CANCEL_BUTTON:String = "clickedCancelButton";
		public static const CLICKED_POWER_OFF_BUTTON:String = "clickedPowerOffButton";
		
		public function ConfirmPowerOffModalViewEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new ConfirmPowerOffModalViewEvent(type,bubbles,cancelable);
		}
	}
}