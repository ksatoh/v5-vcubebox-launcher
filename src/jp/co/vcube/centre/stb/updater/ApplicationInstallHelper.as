package jp.co.vcube.centre.stb.updater
{
	import flash.desktop.NativeApplication;
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
	
	[Event(name="versionFixed", type="jp.co.vcube.centre.stb.updater.ApplicationInstallHelper")]
	[Event(name="notFoundInstaller", type="jp.co.vcube.centre.stb.updater.ApplicationInstallHelper")]
	
	public class ApplicationInstallHelper extends EventDispatcher
	{
		private var versionCallStartInfo:NativeProcessStartupInfo;
		private var nProc:NativeProcess;
		private var currentUpdaterVersion:String="";
		
		public static const VersionFixed:String = "versionFixed";
		public static const NotFoundInstaller:String = "notFoundInstaller";
		
		public function ApplicationInstallHelper()
		{
			super();
		}

		protected function get file():File
		{
			var sep:String = File.separator;
			var a:Array = File.applicationDirectory.nativePath.split(sep);
			a.pop();
			a.push( "tool" );
			if(OS.isCorrectPratform())
			{
				a.push( "VcubeBoxInstaller.exe" );
			}
			else
			{
				a.push( "RebootCenterSTB_mac.exe" );
			}
			return new File( a.join(sep) );
			
		}
		
		public function reboot():void
		{
			if( NativeProcess.isSupported && OS.isCorrectPratform() )
			{
				VCubeBoxUtil.log("send shutdown command via reboot");
				var np:NativeProcess = new NativeProcess();
				var vpInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
				var processArgs:Vector.<String> = new Vector.<String>(); 
				processArgs.push("-r");
				vpInfo.executable = file;
				vpInfo.arguments = processArgs; 
				if ( file && file.exists )
					np.start( vpInfo );
				else
					dispatchEvent( new Event(NotFoundInstaller) );
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
		public function shutdown():void
		{
			if( NativeProcess.isSupported && OS.isCorrectPratform() )
			{
				VCubeBoxUtil.log("send shutdown command via NativeProcess");
				var np:NativeProcess = new NativeProcess();
				var vpInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
				var processArgs:Vector.<String> = new Vector.<String>(); 
				processArgs.push("-s"); 
				processArgs.push("-w"); 
				if ( file && file.exists )
				{
					vpInfo.executable = file;
					vpInfo.arguments = processArgs;
					np.start( vpInfo );
				}
				else
					dispatchEvent( new Event(NotFoundInstaller) );
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
		public function updateWithReboot(ver:String):void
		{
			if( NativeProcess.isSupported && OS.isCorrectPratform() )
			{
				VCubeBoxUtil.log("send update With Reboot command via NativeProcess ",ver );
				var np:NativeProcess = new NativeProcess();
				var vpInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
				var processArgs:Vector.<String> = new Vector.<String>(); 
				processArgs.push("-ur");
				processArgs.push(ver);
				processArgs.push("-w"); 
				
				if ( file && file.exists )
				{
					vpInfo.executable = file;
					vpInfo.arguments = processArgs;
					VCubeBoxUtil.log("native process will called from AIR",vpInfo.executable.nativePath);
					np.start( vpInfo );
				}
				else
					dispatchEvent( new Event(NotFoundInstaller) );
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
		public function updateWithShutdown( ver:String ):void
		{
			if( NativeProcess.isSupported && OS.isCorrectPratform() )
			{
				VCubeBoxUtil.log("send update With shutdown command via NativeProcess ",ver );
				var np:NativeProcess = new NativeProcess();
				var vpInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo();
				var processArgs:Vector.<String> = new Vector.<String>(); 
				processArgs.push("-us");
				processArgs.push(ver);
				if ( file && file.exists )
				{
					vpInfo.executable = file;
					vpInfo.arguments = processArgs;
					np.start( vpInfo )
				}
				else
					dispatchEvent( new Event(NotFoundInstaller) );
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
	}
}