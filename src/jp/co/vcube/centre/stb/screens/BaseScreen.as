package jp.co.vcube.centre.stb.screens {
	
	import flash.utils.Timer;
	
	import jp.co.vcube.centre.stb.core.MainModelLocator;
	import jp.co.vcube.centre.stb.core.VBoxLauncherModel;
	import jp.co.vcube.centre.stb.framework.core.VCSSFocusableScreen;
	import jp.co.vcube.centre.stb.screens.view.ScreenDelegate;
	import jp.co.vcube.centre.stb.utils.VCSSUICreator;
	
	import qnx.ui.core.ContainerAlign;
	import qnx.ui.core.ContainerFlow;
	import qnx.ui.core.SizeUnit;
	
	public class BaseScreen extends VCSSFocusableScreen {
		
		protected const CF_H:String = ContainerFlow.HORIZONTAL;
		protected const CF_V:String = ContainerFlow.VERTICAL;
		protected const CA_N:String = ContainerAlign.NEAR;
		protected const CA_M:String = ContainerAlign.MID;
		protected const CA_F:String = ContainerAlign.FAR;
		protected const SU_R:String = SizeUnit.PERCENT;
		protected const SU_P:String = SizeUnit.PIXELS;
		
		protected var mainModel:MainModelLocator;
		protected var vboxModel:VBoxLauncherModel;
		protected var viewDelegate:ScreenDelegate;
		protected var refreshFocusTimer:Timer;
		
		public function BaseScreen(...initialParams)
		{
			super();
			
			initialize.apply( this, initialParams );
/*
			//リモコンキーと関数の紐付けを行う
			focusManager.setKeybind(this,"MENU",operationMenu);
			focusManager.setKeybind(this,"UP",operationUp);
			focusManager.setKeybind(this,"DOWN",operationDown);
			focusManager.setKeybind(this,"LEFT",operationLeft);
			focusManager.setKeybind(this,"RIGHT",operationRight);
			focusManager.setKeybind(this,"ENTER",operationEnter);
			focusManager.setKeybind(this,"CANCEL",operationCancel);
			focusManager.setKeybind(this,"MIC_MUTE",operationMicMute);
			focusManager.setKeybind(this,"LAYOUT",operationLayout);
			focusManager.setKeybind(this,"EXIT",operationExit);
			focusManager.setKeybind(this,"POWER_OFF",operationPowerOff);
			focusManager.setKeybind(this,"RESET",operationReset);
			*/
		}
		
		override protected function prepareScreen():void {
			
			mainModel = MainModelLocator.getInstance();
			vboxModel = VBoxLauncherModel.getInstance();
			
			flow = CF_V;
			align = CA_M;
		}
		
		override protected function createChildren():void {
			
			super.createChildren();
			
			viewDelegate.exec();
		}
		
		protected function recreateScreen():void {
			
			removeAllChildren();
			
			prepareScreen();
			createChildren();
			prepareChildren();
			
			layout();
		}
		
		/**
		 * 初期化（生成時の引数を適用する）
		 */
		protected function initialize(...initialParams):void {}
/*		
		override public function didStackPush():void {
			
			super.didStackPush();
			updateFocusableContents();
		}
		
		override public function didModal():void {
			
			super.didModal();
			updateFocusableContents();
		}
		
		public override function operationUp():void { super.operationUp(); }
		public override function operationDown():void { super.operationDown(); }
		public override function operationRight():void { super.operationRight(); }
		public override function operationLeft():void { super.operationLeft(); }
		public override function operationEnter():void { super.operationEnter(); }
		public override function operationCancel():void{ super.operationCancel(); }
		public override function operationMenu():void{}
		public function operationHome():void{};
		public function operationNum1():void{};
		public function operationNum2():void{};
		public function operationNum3():void{};
		public function operationNum4():void{};
		public function operationNum5():void{};
		public function operationNum6():void{};
		public function operationNum7():void{};
		public function operationNum8():void{};
		public function operationNum9():void{};
*/		
		/**
		 * フォーカス対象のコンテンツを更新する
		 */
//		protected function updateFocusableContents():void {
//			
//			refreshFocusableContentsByNewContents( viewDelegate.focusableContents );
//		}
		
		protected var createPrimaryLabelButton:Function = VCSSUICreator.createPrimaryLabelButton;
		protected var createSecondryLabelButton:Function = VCSSUICreator.createSecondryLabelButton;
		protected var createLabel:Function = VCSSUICreator.createLabel;
		protected var createCheckBox:Function = VCSSUICreator.createCheckBox;
		
		/**
		 * フォーカス対象を変更する（アニメーションを切っています）
		 */
//		override protected function updateFocusTarget(useAnimation:Boolean=true):void {
//			
//			super.updateFocusTarget( false );
//		}
		
		/**
		 * フォーカス対象のコンテンツを更新する（反映されるのは一定時間後）
		 */
//		override protected function refreshFocusableContentsByNewContents(value:Vector.<IVCSSFocusableContent>):void {
//			
//			if ( refreshFocusTimer )
//				refreshFocusTimer.reset();
//			else
//				refreshFocusTimer = new Timer( 500, 1 );
//			
//			var superRefreshFocusableContentsByNewContents:Function = super.refreshFocusableContentsByNewContents;
//			
//			var f:Function = function():void {
//				
//				refreshFocusTimer.removeEventListener( TimerEvent.TIMER_COMPLETE, f );
//				f = null;
//				superRefreshFocusableContentsByNewContents( value );
//			};
//			
//			refreshFocusTimer.addEventListener( TimerEvent.TIMER_COMPLETE, f );
//			refreshFocusTimer.start();
//		}
		
		/**
		 * フォーカス対象のコンテンツを更新する（即時反映）
		 */
//		protected function refreshFocusableContentsByNewContentsNow(value:Vector.<IVCSSFocusableContent>):void {
//			
//			if ( refreshFocusTimer )
//				refreshFocusTimer.reset();
//			
//			super.refreshFocusableContentsByNewContents( value );
//		}
		
		/**
		 * 現在表示中のスクリーンかどうか
		 */
//		protected function get isCurrentScreen():Boolean {
//			
//			return app.viewStack.length && app.viewStack[ app.viewStack.length - 1 ] == this;
//		}
	}
}
