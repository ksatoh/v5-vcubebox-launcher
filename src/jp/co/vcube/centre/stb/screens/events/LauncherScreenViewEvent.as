package jp.co.vcube.centre.stb.screens.events
{
	import flash.events.Event;
	
	public final class LauncherScreenViewEvent extends Event
	{
		public static const CLICKED_START_MEETING_BUTTON:String = "clickedStartMeetingButton";
		public static const CLICKED_START_SEMINAR_BUTTON:String = "clickedStartSeminarButton";
		public static const CLICKED_POWER_OFF_BUTTON:String = "clickedPowerOffButton";
		
		public function LauncherScreenViewEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new LauncherScreenViewEvent(type,bubbles,cancelable);
		}
	}
}