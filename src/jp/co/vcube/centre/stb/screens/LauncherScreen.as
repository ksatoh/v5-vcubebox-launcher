
package jp.co.vcube.centre.stb.screens {
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.display.StageDisplayState;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.Timer;
	
	import jp.co.vcube.centre.stb.core.NativeWindowKeepActivater;
	import jp.co.vcube.centre.stb.core.VBoxLauncherModel;
	import jp.co.vcube.centre.stb.events.ApplicationShutdownEvent;
	import jp.co.vcube.centre.stb.modalScreens.ConfirmPowerOffModalScreen;
	import jp.co.vcube.centre.stb.screens.events.LauncherScreenViewEvent;
	import jp.co.vcube.centre.stb.screens.view.LauncherScreenDelegate;
	
	public class LauncherScreen extends BaseScreen {
		
		private var model:VBoxLauncherModel;
		
		public function LauncherScreen() {
			
			super();
			
			model = VBoxLauncherModel.getInstance();
		}
		
		override protected function createChildren():void {
			
			viewDelegate = new LauncherScreenDelegate(this);
			
			super.createChildren();
			
			view.addEventListener(LauncherScreenViewEvent.CLICKED_START_SEMINAR_BUTTON,clickStartSeminarButtonHandler);
			view.addEventListener(LauncherScreenViewEvent.CLICKED_START_MEETING_BUTTON,clickStartMeetingButtonHandler);
			view.addEventListener(LauncherScreenViewEvent.CLICKED_POWER_OFF_BUTTON,clickPowerButtonHandler);
		}
		
		///////// HomeScreenDelegate and other event handlers //////////
		
		protected function clickPowerButtonHandler(event:LauncherScreenViewEvent):void
		{
			app.presentModalView( new ConfirmPowerOffModalScreen(), false );
		}
		
		protected function clickStartMeetingButtonHandler(event:LauncherScreenViewEvent):void {
			
			deactivateWindow();
			
			this.visible = false;
			model.backgroundImage.changeVisible(false);
			
			var timer:Timer = new Timer(1000,1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE,function(event:TimerEvent):void {
			
				var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo(); 
				var file:File = File.documentsDirectory.resolvePath( model.utilityLoader.vboxPath );
				nativeProcessStartupInfo.executable = file; 
				var process:NativeProcess = new NativeProcess();
				process.start(nativeProcessStartupInfo);
			});
			timer.start();
		}
		
		protected function clickStartSeminarButtonHandler(event:LauncherScreenViewEvent):void {
			
			deactivateWindow();
			
			var urlReq:URLRequest = new URLRequest( model.utilityLoader.seminarURL ); 
			navigateToURL(urlReq,"brank");
		}
		
		///////// Screen stacking methods //////////
		
		private function setEventHandler():void
		{
			removeEventHandler();
			
			model.addEventListener(ApplicationShutdownEvent.WILL_SHUTDOWN,willShutdownHandler);
		}
		
		private function removeEventHandler():void
		{
			model.removeEventListener(ApplicationShutdownEvent.WILL_SHUTDOWN,willShutdownHandler);
		}
		
		override public function willStackPush():void {
			
			super.willStackPush();
			
			setEventHandler();
		}
		
		override public function stackResume():void {
			
			super.stackResume();
			
			setEventHandler();
			
			mainModel.showStageVideo = true;
		}
		
		override public function willStackPop():void {
			
			super.willStackPop();
			
			pause();
		}
		
		override public function stackPause():void {
			
			super.stackPause();
			
			pause();
		}
		
		override public function didStackPush():void {
			
			super.didStackPush();
			
			mainModel.showStageVideo = true;
		}
		
		private function pause():void
		{
			removeEventHandler();
			
			mainModel.showStageVideo = false;
		}
		
		///////// Information update methods //////////
		
		protected function willShutdownHandler(event:ApplicationShutdownEvent):void
		{
			removeEventHandler();
		}
				
		///////// Key/Remocon event handlers //////////
		
		private function deactivateWindow():void
		{
			NativeWindowKeepActivater.getInstance().stop();
			stage.nativeWindow.alwaysInFront = false;
			stage.displayState = StageDisplayState.NORMAL;
		}
		
		override public function operationPowerOff():void { showConfirmPowerOffModal(); }
		private function showConfirmPowerOffModal():void { app.presentModalView( new ConfirmPowerOffModalScreen(), false ); }
		
		override protected function draw():void {
			
			super.draw();
			
			view.layout();
		}
		
		private function get view():LauncherScreenDelegate { return viewDelegate as LauncherScreenDelegate; }
	}
}

