package jp.co.vcube.centre.stb.screens.view
{
	import flash.events.MouseEvent;
	
	import jp.co.vcube.centre.stb.core.MainModelLocator;
	import jp.co.vcube.centre.stb.framework.core.IVCSSFocusableContent;
	import jp.co.vcube.centre.stb.framework.ui.VCSSButton;
	import jp.co.vcube.centre.stb.framework.ui.VCSSLabel;
	import jp.co.vcube.centre.stb.framework.ui.VCSSLabelButton;
	import jp.co.vcube.centre.stb.screens.LauncherScreen;
	import jp.co.vcube.centre.stb.screens.events.LauncherScreenViewEvent;
	import jp.co.vcube.centre.stb.skins.SkinBase;
	import jp.co.vcube.centre.stb.utils.UIConfig;
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
	import jp.co.vcube.mobile.framework.core.VCContainer;
	
	import qnx.ui.core.Containment;
	
	[Event(name="clickedStartMeetingButton", type="jp.co.vcube.centre.stb.screens.events.LauncherScreenViewEvent")]
	[Event(name="clickedStartSeminarButton", type="jp.co.vcube.centre.stb.screens.events.LauncherScreenViewEvent")]
	[Event(name="clickedPowerOffButton", type="jp.co.vcube.centre.stb.screens.events.LauncherScreenViewEvent")]
	
	public final class LauncherScreenDelegate extends ScreenDelegate
	{
		// static contents
		private var titleLabel:VCSSLabel;
		
		// focusable contents
		private var startMeetingButton:VCSSLabelButton;
		private var StartSeminarButton:VCSSLabelButton;
		private var powerOffButton:VCSSButton;
		
		// containers
		private var infoTitleContainer:VCContainer;
		private var infoContainer:VCContainer;
		private var mainButtonContainer:VCContainer;
		private var infoContainerBody:VCContainer;
		
		private function get LIST_WIDTH():uint { return VCubeBoxUtil.optimizeSize(1280); }
		private function get MAIN_BUTTON_WIDTH():uint { return VCubeBoxUtil.optimizeSize(620); }
		private function get MAIN_BUTTON_HEIGHT():uint { return VCubeBoxUtil.optimizeSize(240); }
		private function get INFO_HEADER_WIDTH():uint { return VCubeBoxUtil.optimizeSize(90); }
		private function get INFO_HEIGHT():uint { return VCubeBoxUtil.optimizeSize(40); }
		private function get MARGIN():uint { return VCubeBoxUtil.optimizeSize(40); }
		private function get POWER_BUTTON_PADDING():uint { return VCubeBoxUtil.optimizeSize(10); }
		
		public function LauncherScreenDelegate(screen:LauncherScreen)
		{
			super(screen);
			
			screen.backgroundVisible = true;
			screen.backgroundColors = [ 0x000000 ];
			screen.backgroundAlphas = [ 0.5 ];
		}
		
		public override function exec():void
		{
			// infoContainer
			infoContainer = new VCContainer();
			infoContainer.flow = CF_H;
			infoContainer.size = 0;
			infoContainer.setSize( MainModelLocator.getInstance().stage.stageWidth, INFO_HEIGHT );
			infoContainer.containment = Containment.UNCONTAINED;
			
			// infoTitleContainer
			infoTitleContainer = new VCContainer();
			infoTitleContainer.align = CA_M;
			infoTitleContainer.size = 0;
			infoTitleContainer.setSize( INFO_HEADER_WIDTH, infoContainer.height );
			infoTitleContainer.backgroundVisible = true;
			infoTitleContainer.backgroundColors = [ 0x999999 ];
			infoTitleContainer.alpha = 0.0;
			
			// infoContainerBody
			infoContainerBody = new VCContainer();
			infoContainerBody.align = CA_N;
			infoContainerBody.size = 0;
			infoContainerBody.setSize( MainModelLocator.getInstance().stage.stageWidth - infoTitleContainer.width, infoContainer.height );
			infoContainerBody.backgroundVisible = true;
			infoContainerBody.backgroundColors = [ 0x000000 ];
			infoContainerBody.backgroundAlphas = [ 0.5 ];
			infoContainerBody.alpha = 0.0;
			
			// mainButtonContainer
			mainButtonContainer = new VCContainer();
			mainButtonContainer.flow = CF_H;
			mainButtonContainer.align = CA_M;
			mainButtonContainer.size = 0;
			mainButtonContainer.setSize( LIST_WIDTH, MAIN_BUTTON_HEIGHT );
			
			// titleLabel
			titleLabel = createLabel( "", UIConfig.fontColorNormal, false, VCubeBoxUtil.fontSizeSmall ); 
			
			// startMeetingButton
			startMeetingButton = SpecialLabelButton.create( "", new SkinBase( "button_StartMeeting" ), MAIN_BUTTON_WIDTH, MAIN_BUTTON_HEIGHT );
			startMeetingButton.addEventListener( MouseEvent.CLICK, clickStartMeetingButtonHandler );
			
			// StartSeminarButton
			StartSeminarButton = SpecialLabelButton.create( "", new SkinBase( "button_InviteMembers" ), MAIN_BUTTON_WIDTH, MAIN_BUTTON_HEIGHT );
			StartSeminarButton.addEventListener( MouseEvent.CLICK, clickStartSeminarButtonHandler );
			
			powerOffButton = new VCSSButton();
			powerOffButton.containment = Containment.UNCONTAINED;
			powerOffButton.setSkin( new SkinBase( "button_PowerOff" ) );
			powerOffButton.addEventListener( MouseEvent.CLICK, clickPowerOffButtonHandler );
			
			// add children to infoTitleContainer
			infoTitleContainer.addChild( createSpacer() );
			infoTitleContainer.addChild( titleLabel );
			infoTitleContainer.addChild( createSpacer() );
			
			// add children to infoContainer
			infoContainer.addChild( infoTitleContainer );
			
			// add chilredn to mainButtonContainer
			mainButtonContainer.addChild( startMeetingButton );
			mainButtonContainer.addChild( createSpacer( LIST_WIDTH - MAIN_BUTTON_WIDTH*2, true ) );
			mainButtonContainer.addChild( StartSeminarButton );
			
			// Uncontained contents
			addChild( infoContainer );
			addChild( powerOffButton );
			
			// Contained contents
			addChild( createSpacer() );
			addChild( mainButtonContainer );
			addChild( createSpacer() );
		}
		
		private function clickStartMeetingButtonHandler(event:MouseEvent):void
		{
			dispatchEvent(new LauncherScreenViewEvent(LauncherScreenViewEvent.CLICKED_START_MEETING_BUTTON));
		}
		
		private function clickStartSeminarButtonHandler(event:MouseEvent):void
		{
			dispatchEvent(new LauncherScreenViewEvent(LauncherScreenViewEvent.CLICKED_START_SEMINAR_BUTTON));
		}
		
		private function clickPowerOffButtonHandler(event:MouseEvent):void
		{
			dispatchEvent(new LauncherScreenViewEvent(LauncherScreenViewEvent.CLICKED_POWER_OFF_BUTTON));
		}
		
		public function layout():void
		{
			powerOffButton.setPosition( screen.width - powerOffButton.width - POWER_BUTTON_PADDING, 
				screen.height - powerOffButton.height - POWER_BUTTON_PADDING );
		}
		
		public override function get focusableContents():Vector.<IVCSSFocusableContent>{
			
			var c:Vector.<IVCSSFocusableContent> = Vector.<IVCSSFocusableContent>( [
				startMeetingButton,
				StartSeminarButton,
				powerOffButton
			] );
			return c;
		}
	}
}

// Private classes

import flash.text.TextFormat;

import jp.co.vcube.centre.stb.framework.ui.VCSSLabelButton;
import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
import jp.co.vcube.mobile.framework.core.VCUtil;

import qnx.ui.skins.SkinStates;
import qnx.ui.skins.UISkin;

class SpecialLabelButton extends VCSSLabelButton {
	
	private var width:Number;
	private var height:Number;
	
	public function SpecialLabelButton( width:Number, height:Number ) {
		
		super();
		
		var tfBlack:TextFormat = VCUtil.createTextFormat();
		tfBlack.color = 0x000000;
		
		var tfWhite:TextFormat = VCUtil.createTextFormat();
		tfWhite.color = 0xffffff;
		
		setTextFormatForState( tfBlack, SkinStates.DISABLED );
		setTextFormatForState( tfBlack, SkinStates.DISABLED_SELECTED );
		setTextFormatForState( tfWhite, SkinStates.DOWN );
		setTextFormatForState( tfWhite, SkinStates.DOWN_SELECTED );
		setTextFormatForState( tfWhite, SkinStates.FOCUS );
		setTextFormatForState( tfWhite, SkinStates.SELECTED );
		setTextFormatForState( tfWhite, SkinStates.UP );
		setTextFormatForState( tfWhite, SkinStates.UP_ODD );
		
		scaleX = scaleY = VCUtil.getOptimizeScale();
		this.width = width;
		this.height = height;
		this.setSize( width, height );
	}
	
	override protected function draw():void {
		
		super.draw();
		label_txt.y = height/2 + VCubeBoxUtil.optimizeSize(20);
	}
	
	public static function create(label:String, skin:UISkin, width:Number, height:Number ):SpecialLabelButton {
		
		var b:SpecialLabelButton = new SpecialLabelButton( width, height );
		b.label = label;
		b.setSkin( skin );
		return b;
	}
}
