package jp.co.vcube.centre.stb.screens.view
{
	import flash.display.DisplayObject;
	import flash.events.FullScreenEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import caurina.transitions.Tweener;
	
	import jp.co.vcube.centre.stb.core.MainModelLocator;
	import jp.co.vcube.centre.stb.core.VBoxLauncherModel;
	import jp.co.vcube.centre.stb.core.events.VBoxLauncherModelEvent;
	import jp.co.vcube.centre.stb.events.MainModelLocatorChangeEvent;
	import jp.co.vcube.centre.stb.skins.Embed;
	import jp.co.vcube.mobile.framework.core.VCContainer;
	import jp.co.vcube.mobile.framework.core.VCScreen;
	
	import qnx.ui.core.Containment;

	public class BackgroundImage
	{
		private var mainScreen:VCScreen;
		private var image:DisplayObject;
		private var defaultImageBounds:Rectangle;
		private var baseContainer:VCContainer;
		private var animationTimer:Timer;
		
		public function BackgroundImage( mainScreen:VCScreen )
		{
			this.mainScreen = mainScreen;
			baseContainer = new VCContainer();
			baseContainer.containment = Containment.BACKGROUND;
			mainScreen.addChild( baseContainer );
		}
		
		protected function setSize(width:Number, height:Number):void
		{
			baseContainer.setSize( width, height );
			
			var r:Number = Math.max( width/defaultImageBounds.width, height/defaultImageBounds.height );
			image.width = r * defaultImageBounds.width;
			image.height = r * defaultImageBounds.height;
			image.x = (width - r * defaultImageBounds.width)/2;
			image.y = (height - r * defaultImageBounds.height)/2;
		}
		
		private function didFullScreenHandler(event:FullScreenEvent):void
		{
			resizeAsFullscreenSize();
		}
		
		private function didChangeScaleHandler(event:VBoxLauncherModelEvent):void
		{
			resizeAsFullscreenSize();
		}
		
		protected function mainModelLocatorChangeEventHandler(event:MainModelLocatorChangeEvent):void {
			
			switch (event.name)
			{
				case MainModelLocator.KIND_SHOW_STAGE_VIDEO:
					if ( !animationTimer )
					{
						animationTimer = new Timer(1000,1);
						animationTimer.addEventListener(TimerEvent.TIMER_COMPLETE,timerCompleteHandler);
					}
					else
						animationTimer.reset();
					animationTimer.start();
					break;
			}
		}
		
		protected function timerCompleteHandler(event:TimerEvent):void
		{
			changeVisible( MainModelLocator.getInstance().showStageVideo );
		}
		
		private function resizeAsFullscreenSize():void
		{
			setSize( mainScreen.stage.fullScreenWidth, mainScreen.stage.fullScreenHeight );
		}
		
		public function show():void
		{
			image = new Embed.backgroundImage();
			baseContainer.addChild( image );
			
			defaultImageBounds = image.getBounds(mainScreen);
			mainScreen.stage.addEventListener(FullScreenEvent.FULL_SCREEN,didFullScreenHandler);
			VBoxLauncherModel.getInstance().addEventListener(VBoxLauncherModelEvent.DID_CHANGE_SCALE,didChangeScaleHandler);
			MainModelLocator.getInstance().addEventListener( MainModelLocatorChangeEvent.VALUE_CHANGED, mainModelLocatorChangeEventHandler );
			resizeAsFullscreenSize();
			
			baseContainer.alpha = 0;
			changeVisible( true );
		}
		
		public function changeVisible( value:Boolean ):void
		{
			Tweener.removeTweens( baseContainer );
			Tweener.addTween(
				baseContainer,
				{
					time : 1.0,
					alpha : value ? 1 : 0
				}
			);
		}
	}
}