package jp.co.vcube.centre.stb.screens.view
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import jp.co.vcube.centre.stb.framework.core.IVCSSFocusableContent;
	import jp.co.vcube.centre.stb.utils.VCSSUICreator;
	import jp.co.vcube.mobile.framework.core.VCScreen;
	
	import qnx.ui.core.ContainerAlign;
	import qnx.ui.core.ContainerFlow;
	import qnx.ui.core.SizeUnit;

	public class ScreenDelegate extends EventDispatcher
	{
		protected var screen:VCScreen;
		
		protected const CF_H:String = ContainerFlow.HORIZONTAL;
		protected const CF_V:String = ContainerFlow.VERTICAL;
		protected const CA_N:String = ContainerAlign.NEAR;
		protected const CA_M:String = ContainerAlign.MID;
		protected const CA_F:String = ContainerAlign.FAR;
		protected const SU_R:String = SizeUnit.PERCENT;
		protected const SU_P:String = SizeUnit.PIXELS;
		
		public function ScreenDelegate(screen:VCScreen)
		{
			this.screen = screen;
		}
		
		public function exec():void{}
		
		protected var createPrimaryLabelButton:Function = VCSSUICreator.createPrimaryLabelButton;
		protected var createSecondryLabelButton:Function = VCSSUICreator.createSecondryLabelButton;
		protected var createLabelButton:Function = VCSSUICreator.createLabelButton;
		protected var createSpacer:Function = VCSSUICreator.createSpacer;
		protected var createLabel:Function = VCSSUICreator.createLabel;
		protected var createCheckBox:Function = VCSSUICreator.createCheckBox;
		
		protected function addChild(child:DisplayObject):void{ screen.addChild(child); }
		public function get focusableContents():Vector.<IVCSSFocusableContent>{ return new Vector.<IVCSSFocusableContent>([]); }
	}
}