package jp.co.vcube.centre.stb.core.events
{
	import flash.events.Event;
	
	public class VBoxLauncherModelEvent extends Event
	{
		public static const DID_CHANGE_SCALE:String = "didChangeScale";
		
		public var data:*;
		
		public function VBoxLauncherModelEvent(type:String, data:*=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.data = data;
		}
		
		public override function clone():Event
		{
			return new VBoxLauncherModelEvent( this.type, data, this.bubbles, this.cancelable );
		}
	}
}