package jp.co.vcube.centre.stb.core
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FullScreenEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import jp.co.vcube.centre.stb.Texts;
	import jp.co.vcube.centre.stb.command.ExternalCommand;
	import jp.co.vcube.centre.stb.core.events.VBoxLauncherModelEvent;
	import jp.co.vcube.centre.stb.events.ApplicationShutdownEvent;
	import jp.co.vcube.centre.stb.events.MainModelLocatorChangeEvent;
	import jp.co.vcube.centre.stb.modalScreens.DuringPowerOffModalScreen;
	import jp.co.vcube.centre.stb.screens.view.BackgroundImage;
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;
	import jp.co.vcube.mobile.framework.core.VCApplication;
	import jp.co.vcube.mobile.framework.core.VCApplicationModalViewStackItem;
	import jp.co.vcube.mobile.framework.core.VCScreen;
	
	[Event(name="willShutdown", type="jp.co.vcube.centre.stb.events.ApplicationShutdownEvent")]

	public class VBoxLauncherModel extends EventDispatcher
	{
		protected static var instance:VBoxLauncherModel;
		public static function getInstance(baseScreen:VCScreen=null):VBoxLauncherModel {
			return instance ?
				instance :
				instance = new VBoxLauncherModel( new InstantiateKey(), baseScreen );
		}
		
		private var baseScreen:VCScreen;
		private var mainModel:MainModelLocator;
		private var defaultStageRect:Rectangle;
		public var backgroundImage:BackgroundImage;
		
		public function VBoxLauncherModel(singletonKey:InstantiateKey, baseScreen:VCScreen=null)
		{
			super();
			
			this.baseScreen = baseScreen;
			
			addEventListener(ApplicationShutdownEvent.WILL_SHUTDOWN,willShutdownHandler);
			NativeApplication.nativeApplication.addEventListener( Event.EXITING, nativeApplicationExitingHandler );
			baseScreen.addEventListener(Event.ADDED_TO_STAGE,addToStageHandler);
			baseScreen.stage.addEventListener(FullScreenEvent.FULL_SCREEN,didFullScreenHandler);
		}
		
		//ExternalCommands.prepareCloseを受ける
		private function willShutdownHandler(event:ApplicationShutdownEvent):void
		{
			var exists:Boolean = false;
			for each ( var item:VCApplicationModalViewStackItem in VCApplication.getInstance().modalViews ) {
				if ( item.view is DuringPowerOffModalScreen )
					exists = true;
			}
			if ( !exists )
				VCApplication.getInstance().presentModalView( new DuringPowerOffModalScreen( Texts.DURING_POWER_OFF ) );
		}
		
		private function nativeApplicationExitingHandler( e:Event ):void
		{
			e.preventDefault();
			ExternalCommand.shutdown();
		}
		
		private function mainModelValueChangedHandler(event:MainModelLocatorChangeEvent):void
		{
			switch (event.name)
			{
				case MainModelLocator.KIND_CHANGE_GATE:
					
					if ( event.value )
					{
						if ( event.oldValue )
							event.oldValue.removeEventListener(MainModelLocatorChangeEvent.VALUE_CHANGED,mainModelValueChangedHandler);
					}
					break;
			}
		}
		
		private function windowCloseCheckHandler(event:TimerEvent):void
		{
			if ( MainModelLocator.getInstance().stage.nativeWindow.closed )
				NativeApplication.nativeApplication.exit();
		}
		
		protected function addToStageHandler(event:Event):void {
			
			//フルスクリーン化する前のスクリーンサイズを保管する
			defaultStageRect = new Rectangle( 0, 0, baseScreen.stage.width, baseScreen.stage.height );
			
			var timer:Timer = new Timer(200,0);
			timer.addEventListener(TimerEvent.TIMER,didResizeHandler);
			timer.start();
		}
		
		
		protected function didFullScreenHandler(event:FullScreenEvent):void
		{
			baseScreen.stage.removeEventListener(FullScreenEvent.FULL_SCREEN,didFullScreenHandler);
			
			resetFullscreenScale();
			baseScreen.setPosition( (defaultStageRect.width - baseScreen.stage.fullScreenWidth)/2, (defaultStageRect.height - baseScreen.stage.fullScreenHeight)/2 );
		}
		
		protected var fullScreenRect:Rectangle = new Rectangle();
		protected function didResizeHandler(event:Event):void
		{
			if ( fullScreenRect.width != baseScreen.stage.fullScreenWidth || fullScreenRect.height != baseScreen.stage.fullScreenHeight )
			{
				baseScreen.setPosition( (defaultStageRect.width - baseScreen.stage.fullScreenWidth)/2, (defaultStageRect.height - baseScreen.stage.fullScreenHeight)/2 );
				baseScreen.setSize( baseScreen.stage.fullScreenWidth, baseScreen.stage.fullScreenHeight );
				resetFullscreenScale();
				
				fullScreenRect.width = baseScreen.stage.fullScreenWidth;
				fullScreenRect.height = baseScreen.stage.fullScreenHeight;
			}
		}
		
		protected function resetFullscreenScale():void
		{
			var newValue:Number = Math.min( baseScreen.stage.fullScreenWidth/defaultStageRect.width, baseScreen.stage.fullScreenHeight/defaultStageRect.height );
			if ( VCubeBoxUtil.fullscreenScale != newValue )
			{
				VCubeBoxUtil.fullscreenScale = newValue;
				dispatchEvent( new VBoxLauncherModelEvent(VBoxLauncherModelEvent.DID_CHANGE_SCALE) );
			}
		}
		
		public var utilityLoader:VBoxLauncherUtilitySettingLoader;
	}
}

class InstantiateKey {}