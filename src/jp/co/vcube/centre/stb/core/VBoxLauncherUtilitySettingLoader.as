package jp.co.vcube.centre.stb.core
{
	public class VBoxLauncherUtilitySettingLoader extends ExternalSettingLoader
	{
		private static var instance:VBoxLauncherUtilitySettingLoader;
		public static function getInstance():VBoxLauncherUtilitySettingLoader{ return instance ? instance : instance = new VBoxLauncherUtilitySettingLoader(new SingletonKey()); }
		
		public function VBoxLauncherUtilitySettingLoader(singletonKey:SingletonKey)
		{
			super();
		}
		
		public function get vboxPath():String
		{
			return getPropertyOf("applicationSettings.vboxPath");
		}
		
		public function get seminarURL():String
		{
			return getPropertyOf("applicationSettings.seminarURL");
		}
		
		////// Utility //////
		public function getPropertyOf( prop:String, src:XML=null ):String
		{
			var _v:String;
			var _p:String = prop.split(".")[0];
			var _n:Array = prop.split(".");
			_n.shift();
			
			if ( !src )
				src = setting;
			
			if ( src.hasOwnProperty(_p) )
			{
				if ( (src.child(_p)[0] as XML).attributes().length() )
					_v = String(src.child(_p)[0].@value);
				else
					_v = getPropertyOf( _n.join("."), src.child(_p)[0] as XML );
				return _v;
			}
			return "";
		}
		public function getBooleanValueOf(prop:String, defaultValue:Boolean=false):Boolean
		{
			switch ( String(getPropertyOf(prop)) )
			{
				case "true":
				case "1":
					return true;
					break;
				case "false":
				case "0":
					return false;
					break;
				default:
					return defaultValue;
					break;
			}
			return defaultValue;
		}
	}
}
class SingletonKey{}