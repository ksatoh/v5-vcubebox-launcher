package jp.co.vcube.centre.stb.core
{
	import jp.co.vcube.centre.stb.command.ExternalCommand;

	public final class PowerOffController
	{
		private static var instance:PowerOffController;
		public static function getInstance():PowerOffController { return instance ? instance : instance = new PowerOffController(new SingletonKey()); }
		
		private var _doing:Boolean = false;
		public function get doing():Boolean { return _doing; }
		
		public function PowerOffController(singletonKey:SingletonKey)
		{
			if (!(singletonKey is SingletonKey))
				return;
		}
		
		public function exec():void
		{
			trace("ExternalCommand.shutdown");
			ExternalCommand.shutdown();
		}
	}
}
class SingletonKey{}