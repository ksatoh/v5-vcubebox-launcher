package jp.co.vcube.centre.stb.command
{
	import flash.utils.Timer;
	
	import jp.co.vcube.centre.stb.core.VBoxLauncherModel;
	import jp.co.vcube.centre.stb.events.ApplicationShutdownEvent;
	import jp.co.vcube.centre.stb.updater.ApplicationInstallHelper;
	import jp.co.vcube.centre.stb.utils.VCubeBoxUtil;

	public class ExternalCommand
	{
		private var closer:Function;
		public function ExternalCommand( key: Key,func:Function )
		{
			closer = func;
		}
		
		public function exec():void { closer(); }
		
		
		public static function shutdown():void
		{
			VCubeBoxUtil.log("willShutdown");
			var f:Function = function():void
			{
				VCubeBoxUtil.log("shutdown");
				var ap:ApplicationInstallHelper = new ApplicationInstallHelper();
				ap.shutdown();
			}
			
			new ExternalCommand( new Key(),f ).prepareClose();
		}
		public static function reboot():void
		{
			VCubeBoxUtil.log("willReboot");
			var f:Function = function():void
			{
				VCubeBoxUtil.log("reboot");
				var ap:ApplicationInstallHelper = new ApplicationInstallHelper();
				ap.reboot();
			}
			
			new ExternalCommand( new Key(),f ).prepareClose();
		}
		
		public static function updateWithReboot( version:String ):void
		{
			VCubeBoxUtil.log("willUpdateWithReboot");
			var f:Function = function():void
			{
				VCubeBoxUtil.log("updateWithReboot");
				var ap:ApplicationInstallHelper = new ApplicationInstallHelper();
				ap.updateWithReboot(version);
			}
			
			new ExternalCommand( new Key(),f ).prepareClose();
		}
		
		public static function updateWithShutdown( version:String ):void
		{
			VCubeBoxUtil.log("willUpdateWithShutdown");
			var f:Function = function():void
			{
				VCubeBoxUtil.log("updateWithShutdown");
				var ap:ApplicationInstallHelper = new ApplicationInstallHelper();
				ap.updateWithShutdown(version);
			}
			
			new ExternalCommand( new Key(),f ).prepareClose();
		}
		
		private var repeatChangeStatusTimer:Timer;
		private var gateClosureTimer:Timer;
		private const GATE_CLOSURE_DELAY:uint = 10 * 1000;
		public function prepareClose():void
		{
			if(!Boolean(closer))return;
			
			VBoxLauncherModel.getInstance().dispatchEvent( new ApplicationShutdownEvent(ApplicationShutdownEvent.WILL_SHUTDOWN) );
			
			exec();
		}
	}
}
class Key{}